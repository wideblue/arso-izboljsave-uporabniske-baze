# METEOROLOŠKA BAZA PODATKOV AMEBA

Meteorološke podatke shranjujemo v podatkovno bazo Ameba, ki se nahaja na tmpvirgi. Vir podatkov je lahko iz klasičnih postaj (padavinske ali podnebne postaje) ali iz samodejnih postaj. Glede na vir so surovi podatki zapisani ali v tabelah za klasične (npr. klima_vhodna) ali samodejne postaje (npr. amp_o). Ko se nad surovimi podatki izvede postopek kontrole, se podatki prepišejo v uporabniške tabele. Več o o tem je bilo že napisano v dokumentu o [bazi meteoroloških podatkov](http://tmpvirga.rzs-hm.si/~mihad/dokumentacija_baza/baza_meteoroloskih_podatkov.htm).

Namen tega dokumenta je, da se popiše tabele, kje je kaj shranjeno.

### PODATKI KLASIČNIH POSTAJ


Podatki so shranjeni v naslednjih tabelah:
* [klima_vhodna](klima_vhodna.md) - klimatološke postaje
* [padavine_vhodna](padavine_vhodna.md) - padavinske postaje
* [pluviografi_vhodna](http://tmpvirga.rzs-hm.si/~mihad/dokumentacija_baza/pluviografi.html) - podatki iz pluviografov
* [trajanje_vhodna](http://tmpvirga.rzs-hm.si/~mihad/dokumentacija_baza/heliografi.html) - podatki iz heligrafov

### PODATKI SAMODEJNIH POSTAJ

Meteorološke podatke na samodejnih postajah se zbira iz različnih mrež, in sicer: meteorološke postaje, ekološke postaje, hidrološke postaje, postaje iz drugih podsistemov in datalogerji. Podatki se beležijo na 10 ali 30 minut in se odvisno od parametra shranjujejo v naslednjih tabelah: [amp_o](amp_o.md),  [amp_p](amp_p.md),  [amp_v](amp_v.md), [amp_p_10min.md](amp_p_10min.md).

Ko so bili podatki še na serverju virga do leta 2010, so se podatki v amp tabele prepisovale iz datotek, ki so jih zgenerirale same postaje (skrbnik Jožef Roškar). Po prehodu na server tmpvirga, smo začeli prepisovati podatke iz serverja Tajfun, kjer je Oracle okolje (skrbnik prenosa je Miha Demšar).

### UPORABNIŠKE TABELE

V uporabniških tabelah so na enem mestu zbrani vsi podatki iz vseh merilnih mest. Vsakemu podatku v tabeli je pripisan indeks verodostojnsti, ki uporabniku govori o kvaliteti podatka. Shema spodaj prikazuje prepis surovih podatkov v uporabniške tabele:
![alt text](http://tmpvirga.rzs-hm.si/~mihad/dokumentacija_baza/tabele_ameba.jpg "Logo Title Text 1")

##### PREPIS PODATKOV IZ KLASIČNIH POSTAJ

Vsakodnevno okoli 20 UTC se prenašajo podatki iz tabel klima_vhodna in padavine_vhodna. Zato skrbita programa:
 /home/mihad/programi/prepis_v_uporabniske/dnevni_klima_pad.pl
 /home/mihad/programi/prepis_v_uporabniske/mesecni_vnos_opazovalci.pl
 
 Zaradi nadaljevanja niza podatkov na spletnem arhiv za ukinjene glavne postaje to skrbi program:
 /home/mihad/programi/prepis_v_uporabniske/amp_v_uporabnisko_klimo/datumi_prepisa.pl
 
Program je napisan tako, da ne kontrolira podatkov na novo, zato ima za indeks verodostojnosti napisano vrednost 16384 (ni kontrole) ali 31744 (manjkajoč) ali 24576 (napčen).

Podatki se prepisujejo v tabele:
terminska_par, dnevna_par, dnevna_padavine, dnevna_pojavi_1, dnevna_pojavi_2, mesecna. 
 
 
 ##### PREPIS PODATKOV IZ SAMODEJNIH POSTAJ
 
 Za prepis podatkov v različne tabele skrbijo različni programi, odvisno od spremenljivke:
 1.)**padavine**: prepis v tabele polurna_padavine, dnevna_padavine
 11 7 * * 0,1,2,3,4,5,6  /home/mihad/programi/kontrola_amp_padavin/kontrola_padavin_nova.pl 
 2.)**temperatura, tlak in veter**:prepis v tabele terminska_par, polurna_ptr
 3.)**relativna vlaga**: prepis v tabele terminska_par, polurna_ptr, dnevna_par
 45 7 * * *  /home/mihad/programi/kontrola_T_p_rh/kontrola_rv.pl
 3.) **globalno in difuzno sevanje**: za program skrbi Gregor Vertačnik
 
 
