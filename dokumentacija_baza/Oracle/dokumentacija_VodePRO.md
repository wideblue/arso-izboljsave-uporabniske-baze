# Dokumentacija za VodePRO

V tem dokumentu bo opis podatkovnega toka, ki se uporablja v aplikaciji vodePRO. Poudarek bo na ozadju - kako se stvari odvijajo na bazah in tabelah, ki se uporabljajo za to aplikacijo.

## Opis delovnega toka v aplikaciji 



## Shema KOLOMON 
### KOLOMON.PAR_TAB
Struktura tabele:


| COLUMN_NAME           | DATA_TYPE          | NULLABLE | DEFAULT_DATA | COLUMN_ID | COMMENTS                                    |
|-----------------------|--------------------|----------|--------------|-----------|---------------------------------------------|
| GRAF_SKALA_MAX        | NUMBER(10,5)       | Yes      | (null)       | 12        | (null)                                      |
| GRAF_SKALA_MIN        | NUMBER(10,5)       | Yes      | (null)       | 11        | (null)                                      |
| ID_PV                 | SE_PRERACUNAM      | Yes      | (null)       | 15        | Spisek NP, ki se preracunavajo              |
| OBRNI_Y               | NUMBER(1,0)        | Yes      | 0            | 17        | (null)                                      |
| PAI_ID_PARAMETRA      | NUMBER(8,0)        | No       | (null)       | 5         | ID ISMMjevskega parametra                   |
| PT_DODATNI_POGOJ      | VARCHAR2(150 BYTE) | Yes      | (null)       | 3         | (null)                                      |
| PT_ENOTA              | VARCHAR2(20 BYTE)  | Yes      | (null)       | 10        | (null)                                      |
| PT_ID_PAR             | NUMBER(38,0)       | No       | (null)       | 1         | id novega parametra                         |
| PT_KRATKI_OPIS        | VARCHAR2(30 BYTE)  | Yes      | (null)       | 6         | Kratka imane za legende                     |
| PT_OPIS               | VARCHAR2(255 BYTE) | Yes      | (null)       | 2         | (null)                                      |
| PT_POMOZNI            | NUMBER(3,0)        | No       | 0            | 7         | (null)                                      |
| TAB_ATR_TA_ID_TAB_ATR | NUMBER(38,0)       | No       | (null)       | 4         | ID iz TABELE_ATRIBUTI                       |
| ZAC                   | NUMBER             | Yes      | (null)       | 13        | (null)                                      |
| ZA_INTERPOLACIJO      | VARCHAR2(1 BYTE)   | No       | 'N'          | 9         | Ali je parameter kandidat za interpolacijo? |
| ZA_KOLOMON            | VARCHAR2(1 BYTE)   | No       | 'D'          | 16        | Ali naj bo NP viden v Kolomonu?             |
| ZA_ROZO               | VARCHAR2(1 BYTE)   | Yes      | (null)       | 8         | (null)                                      |
| ZA_UREJANJE           | VARCHAR2(1 BYTE)   | No       | 'N'          | 4         | (null)                                      |


Opis tabele: 
V tabeli so relacije med 
postajami in parametri.


### KOLOMON.PAR_TAB
Struktura tabele:

| COLUMN_NAME      | DATA_TYPE          | NULLABLE | DEFAULT_DATA | COLUMN_ID | COMMENTS                                               |
|------------------|--------------------|----------|--------------|-----------|--------------------------------------------------------|
| KAR_PAR_1        | VARCHAR2(1 BYTE)   | No       | 'N'          | 6         | karakteristicen parameter za prikaz                    |
| TABELE_TABELE_ID | NUMBER(38,0)       | No       | (null)       | 5         | (null)                                                 |
| TA_ID_TAB_ATR    | NUMBER(38,0)       | No       | (null)       | 1         | (null)                                                 |
| TA_IME_DATUMSKE  | VARCHAR2(30 BYTE)  | No       | (null)       | 3         | (null)                                                 |
| TA_IME_VREDNOSTI | VARCHAR2(30 BYTE)  | No       | (null)       | 2         | Ime stolpca v tabeli                                   |
| TA_OPIS          | VARCHAR2(100 BYTE) | Yes      | (null)       | 4         | Lep opis parametra                                     |
| TA_TIP_VRED      | NUMBER(38,0)       | Yes      | 1            | 7         | tip vrednosti (1=trenutna, 2, povprecna, 3=max, 4=min) |


Opis tabele: 
V tabeli so relacije med parametri in 
lokacijami hrambe.

### KOLOMON.POSTAJE_PARAMETRI
Struktura tabele:

| COLUMN_NAME      | DATA_TYPE        | NULLABLE | DEFAULT_DATA | COLUMN_ID | COMMENTS                |
|------------------|------------------|----------|--------------|-----------|-------------------------|
| AKTIVEN          | CHAR(1 BYTE)     | No       | 'D'          | 8         | (null)                  |
| AMP_PAR          | NUMBER(38,0)     | Yes      | (null)       | 7         | (null)                  |
| AMP_SIF          | VARCHAR2(6 BYTE) | Yes      | (null)       | 6         | (null)                  |
| MM_ID_ISMM       | NUMBER(11,0)     | No       | (null)       | 1         | Id sistema ISMM         |
| PAR_ID_PARAMETRA | NUMBER(11,0)     | No       | (null)       | 2         | ismmjevski id_parametra |
| PP_JAVNOST       | VARCHAR2(1 BYTE) | No       | 'N'          | 5         | Primerna za javnost     |
| PP_PERIODA       | NUMBER(38,0)     | Yes      | (null)       | 3         | Perioda podatkov        |
| PP_TESTNA        | VARCHAR2(1 BYTE) | No       | 'N'          | 4         | Testne meritve          |


Opis tabele:


-------------------- start notes 

vse te tabele uporabljajo -----
TABELE_ATRIBUTI (relacije med parametri in 
lokacijami hrambe) , PARAMETRI in POSTAJE
--------------- end notes 


## Shema ISMM

### ISMM.MERILNA_MESTA 
Struktura tabele:





------------- start notes 
V ISMM shemi nam je najbolj pomembna tabela MERILNA_MESTA (register), pa 
tudi PARAMETRI (sifrant parametrov) in VELICINE (sifrant količin).

--------------
----opomba:

Najbolj zanimiva tabela v ISMM je ISMM_PART, ne MERILNA_MESTA. 
MERILNA_MESTA je samo hard kopija vnosov iz ISMM_PART, ki so se dotikali 
naših vsebin.

------ end notes


## Shema AMP


----------- start notes
V AMP tabeli pa so podatki razmetani po precej tabelah, ampak  konkretne lokacije podatkov iščemo z viewjem VW_MM_NP

---------- end notes


## Shema VODE_PRO


### VODE_PRO.XX 
Struktura tabele:

### VODE_PRO.XX 
Struktura tabele:


### VODE_PRO.XX 
Struktura tabele:



### VIEW.VW_MM_NP


Rezultat tega pogleda je tabela:

| COLUMN_NAME      | DATA_TYPE      | NULLABLE | DATA_DEFAULT | COLUMN_ID | COMMENTS | INSERTABLE | UPDATABLE | DELETABLE |
|------------------|----------------|----------|--------------|-----------|----------|------------|-----------|-----------|
| KLA_SIF          | VARCHAR2(10)   | No       | (null)       | 1         | (null)   | NO         | NO        | NO        |
| AMP_SIF          | VARCHAR2(10)   | No       | (null)       | 2         | (null)   | NO         | NO        | NO        |
| OEA_ID_OE        | NUMBER(11)     | No       | (null)       | 3         | (null)   | NO         | NO        | NO        |
| PART_VODOTOK     | VARCHAR2(100)  | Yes      | (null)       | 4         | (null)   | NO         | NO        | NO        |
| PART_IME         | VARCHAR2(100)  | No       | (null)       | 5         | (null)   | NO         | NO        | NO        |
| GEOMETRY         | SDO_GEOMETRY() | Yes      | (null)       | 6         | (null)   | YES        | YES       | YES       |
| OBJECTID         | NUMBER(10)     | No       | (null)       | 7         | (null)   | NO         | NO        | NO        |
| AMP_PAR          | NUMBER(38)     | Yes      | (null)       | 8         | (null)   | NO         | NO        | NO        |
| ID_NP            | NUMBER(38)     | No       | (null)       | 9         | (null)   | NO         | NO        | NO        |
| PT_OPIS          | VARCHAR2(255)  | Yes      | (null)       | 10        | (null)   | NO         | NO        | NO        |
| PAI_ID_PARAMETRA | NUMBER(8)      | No       | (null)       | 11        | (null)   | NO         | NO        | NO        |
| PT_KRATKI_OPIS   | VARCHAR2(30)   | Yes      | (null)       | 12        | (null)   | NO         | NO        | NO        |
| PT_ENOTA         | VARCHAR2(20)   | Yes      | (null)       | 13        | (null)   | NO         | NO        | NO        |
| ID_VELICINE      | NUMBER(11)     | No       | (null)       | 14        | (null)   | NO         | NO        | NO        |
| VELICINA         | VARCHAR2(255)  | Yes      | (null)       | 15        | (null)   | NO         | NO        | NO        |
| OPIS             | VARCHAR2(255)  | Yes      | (null)       | 16        | (null)   | NO         | NO        | NO        |
| PT_DODATNI_POGOJ | VARCHAR2(150)  | Yes      | (null)       | 17        | (null)   | NO         | NO        | NO        |
| TA_ID_TAB_ATR    | NUMBER(38)     | No       | (null)       | 18        | (null)   | NO         | NO        | NO        |
| TA_IME_VREDNOSTI | VARCHAR2(30)   | No       | (null)       | 19        | (null)   | NO         | NO        | NO        |
| TA_IME_DATUMSKE  | VARCHAR2(30)   | No       | (null)       | 20        | (null)   | NO         | NO        | NO        |
| TA_TIP_VRED      | NUMBER(38)     | Yes      | (null)       | 21        | (null)   | NO         | NO        | NO        |
| TABELE_ID        | NUMBER(38)     | No       | (null)       | 22        | (null)   | NO         | NO        | NO        |
| TABELE_IME       | VARCHAR2(50)   | No       | (null)       | 23        | (null)   | NO         | NO        | NO        |
| TABELE_SHEMA     | VARCHAR2(50)   | No       | (null)       | 24        | (null)   | NO         | NO        | NO        |

Namen viewja je, da se poišče konkretne lokacije merjenih podatkov.

QUERY:

``` sql

  CREATE OR REPLACE FORCE VIEW "VODE_PRO"."VW_MM_NP" ("KLA_SIF", "AMP_SIF", "OEA_ID_OE", "PART_VODOTOK", "PART_IME", "GEOMETRY", "OBJECTID", "AMP_PAR", "ID_NP", "PT_OPIS", "PAI_ID_PARAMETRA", "PT_KRATKI_OPIS", "PT_ENOTA", "ID_VELICINE", "VELICINA", "OPIS", "PT_DODATNI_POGOJ", "TA_ID_TAB_ATR", "TA_IME_VREDNOSTI", "TA_IME_DATUMSKE", "TA_TIP_VRED", "TABELE_ID", "TABELE_IME", "TABELE_SHEMA") AS 
  
  SELECT MERILNA_MESTA.KLA_SIF,
    MERILNA_MESTA.AMP_SIF,
    MERILNA_MESTA.OEA_ID_OE,
    MERILNA_MESTA.PART_VODOTOK,
    MERILNA_MESTA.PART_IME,
    MERILNA_MESTA.geometry,
    MERILNA_MESTA.objectid,
    KOLOMON.POSTAJE_PARAMETRI.AMP_PAR,
    KOLOMON.PAR_TAB.PT_ID_PAR ID_NP,
    KOLOMON.PAR_TAB.PT_OPIS,
    KOLOMON.PAR_TAB.PAI_ID_PARAMETRA,
    KOLOMON.PAR_TAB. PT_KRATKI_OPIS,
    KOLOMON.PAR_TAB.PT_ENOTA,
    VELICINE.ID_VELICINE,
    VELICINE.VELICINA,
    VELICINE.OPIS,
    KOLOMON.PAR_TAB.PT_DODATNI_POGOJ,
    KOLOMON.TABELE_ATRIBUTI.TA_ID_TAB_ATR,
    KOLOMON.TABELE_ATRIBUTI.TA_IME_VREDNOSTI,
    KOLOMON. TABELE_ATRIBUTI.TA_IME_DATUMSKE,
    KOLOMON.TABELE_ATRIBUTI.TA_TIP_VRED,
    KOLOMON.TABELE.TABELE_ID,
    KOLOMON.TABELE.TABELE_IME,
    KOLOMON.TABELE.TABELE_SHEMA
  
  FROM VODE_PRO.MERILNA_MESTA,
    KOLOMON.PAR_TAB,
    KOLOMON.POSTAJE_PARAMETRI,
    KOLOMON.TABELE_ATRIBUTI,
    KOLOMON.TABELE,
    ISMM.PARAMETRI,
    ISMM.VELICINE
  
  WHERE ( KOLOMON.POSTAJE_PARAMETRI.PAR_ID_PARAMETRA = KOLOMON.PAR_TAB.PAI_ID_PARAMETRA )
  AND ( KOLOMON.PAR_TAB.TAB_ATR_TA_ID_TAB_ATR        = KOLOMON.TABELE_ATRIBUTI.TA_ID_TAB_ATR )
  AND ( KOLOMON.TABELE_ATRIBUTI. TABELE_TABELE_ID    = KOLOMON.TABELE.TABELE_ID )
  AND ( MERILNA_MESTA.AMP_SIF                        = POSTAJE_PARAMETRI.AMP_SIF)
  AND ( KOLOMON.POSTAJE_PARAMETRI.PAR_ID_PARAMETRA   = parametri.id_parametra)
  AND (parametri.id_velicine = velicine.id_velicine);

```




### VIEW.VW_PODATKI_ZADNJI_AMP_H

Rezultat tega pogleda je tabela:



Namen viewja je, da se poišče veliko podatkov za vsako postajo (opozorilne vrednosti, zadnji podatki, vodilne količine).

QUERY:

``` sql

CREATE OR REPLACE FORCE VIEW "VODE_PRO"."VW_PODATKI_ZADNJI_AMP_H" ("KLA_SIF", "AMP_SIF", "PART_IME", "PART_VODOTOK", "WGS84_DOLZINA", "WGS84_SIRINA", "AKTIVNA", "Q_AMP_PAR", "Q_SCHEME", "Q_TABLE", "Q_DATUM", "Q_VRED", "H_AMP_PAR", "H_SCHEME", "H_TABLE", "H_DATUM", "H_VRED", "TV_AMP_PAR", "T_SCHEME", "T_TABLE", "T_DATUM", "T_VRED", "SUSNI_PRETOK", "MALI_PRETOK", "OBICAJNI_PRETOK", "VELIK_PRETOK", "Q_RUMENI", "Q_ORANZNI", "Q_RDECI", "HA_ID", "HA_NAME", "TENDENCA_ZADNJA_MERITEV", "TENDENCA", "Q_ZNACILNA_VREDNOST", "Q_ZNACILNA_OPIS", "SHNP", "SHSR", "SHVP", "RUMENI_H", "ORANZNI_H", "RDECI_H", "VODILNA_KOLICINA", "IP_KAMERE", "Q_MANJKA", "H_MANJKA", "T_MANJKA", "TEND_MANJKA", "ACTIVE_H", "ACTIVE_Q", "ACTIVE_TV", "SUSNI_H", "MALI_H", "OBICAJNI_H", "VELIKI_H", "Q_KOLOMON", "H_KOLOMON", "TV_KOLOMON") AS 
  select
    kla_sif
, amp_sif
, part_ime
, part_vodotok
, wgs84_dolzina
, wgs84_sirina
, aktivna
, q_amp_par, q_scheme, q_table, q_datum, q_vred
, h_amp_par, h_scheme, h_table, h_datum, h_vred
, tv_amp_par, t_scheme, t_table, t_datum, t_vred
, susni_pretok
, mali_pretok
, obicajni_pretok
, velik_pretok
, q_rumeni
, q_oranzni
, q_rdeci
, ha_id
, ha_name
, tendenca_zadnja_meritev
, tendenca
, q_znacilna_vrednost
,opis_znacilne as q_znacilna_opis
,shnp, shsr, shvp, rumeni_h, oranzni_h, rdeci_h
,vodilna_kolicina
, ip_kamere
,case
      when q_datum is null then null
      when round(24 * (sysdate + 1/24- q_datum), 1) >1 
      then round(24 * (sysdate + 1/24 - q_datum), 0) 
      else 0 end  AS Q_MANJKA
,case
      when h_datum is null then null
      when round(24 * (sysdate + 1/24- h_datum), 1) >1 
      then round(24 * (sysdate + 1/24 - h_datum), 0) 
      else 0 end AS H_MANJKA
,case
      when t_datum is null then null
       when round(24 * (sysdate + 1/24- t_datum), 1) >1 
      then round(24 * (sysdate + 1/24 - t_datum), 0) 
      else 0 end  AS T_MANJKA     
,case
      when tendenca_zadnja_meritev is null then null
      when round(24 * (sysdate + 1/24 - tendenca_zadnja_meritev), 1) >1 
      then round(24 * (sysdate + 1/24- tendenca_zadnja_meritev), 0) 
      else 0 end  AS TEND_MANJKA 
,case 
        when h_id is null then 'F'
        else 'T' end as active_h
,case 
        when q_id is null then 'F'
        else 'T' end as active_q
,case 
        when tv_id is null then 'F'
        else 'T' end as active_tv
,susni_h, mali_h, obicajni_h, veliki_h
,q_kolomon
,h_kolomon
,tv_kolomon
from(
select  
 kla_sif
, amp_sif
, part_ime
, part_vodotok
, wgs84_dolzina
, wgs84_sirina
, aktivna
, q_amp_par, q_scheme, q_table, q_datum, q_vred
, h_amp_par, h_scheme, h_table, h_datum, h_vred
, tv_amp_par, t_scheme, t_table, t_datum, t_vred
, susni_pretok
, mali_pretok
, obicajni_pretok
, velik_pretok
, rumeni_alarm as q_rumeni
, oranzni_alarm as q_oranzni
, rdeci_alarm as q_rdeci
, ha_id
, ha_name
, zadnja_meritev as tendenca_zadnja_meritev
, tendenca
--, (select f_znacilne_vrednosti_new(kla_sif, 4, q_vred) from dual) AS Q_ZNACILNA_VREDNOST
, (select f_znacilne_vrednosti(kla_sif, 4, q_vred) from dual) AS Q_ZNACILNA_VREDNOST
, ip_kamere
,shnp, shsr, shvp, susni_h, mali_h, obicajni_h, veliki_h, rumeni_h, oranzni_h, rdeci_h
, vodilna_kolicina, h_id, q_id, tv_id 
,q_kolomon, h_kolomon, tv_kolomon
from (
/**
ALL STATIONS minus OTT stations
**/
select kla_sif, amp_sif, part_ime, part_vodotok, wgs84_dolzina, wgs84_sirina, q_amp_par, q_scheme, q_table, q_datum, q_vred, h_amp_par, h_scheme, h_table, h_datum, h_vred, tv_amp_par, t_scheme, t_table, t_datum, t_vred
--,q_kolomon, h_kolomon, tv_kolomon 
, f_get_kolomon(amp_sif, q_amp_par, q_datum, q_table) as q_kolomon
, f_get_kolomon(amp_sif, h_amp_par, h_datum, h_table) as h_kolomon
, f_get_kolomon(amp_sif, tv_amp_par, t_datum, t_table) as tv_kolomon
from(
select kla_sif, amp_sif, part_ime, part_vodotok, wgs84_dolzina, wgs84_sirina from merilna_mesta where oea_id_oe=4 and amp_sif not in('H50', 'H52', 'H58', 'H439')) postaje
left join(
select amp_sif as amp_sifra, q_scheme, q_table, q_amp_par, h_scheme, h_table, h_amp_par, t_scheme, t_table, tv_amp_par from vode_pro.vw_postaje_par_in_podatki
) lokacija
on postaje.amp_sif=lokacija.amp_sifra
left join(
select datum as q_datum, postaja, parameter, vred as q_vred, status_kolomon as q_kolomon from amp.podatki_zadnji
) q
on postaje. amp_sif = q.postaja and
lokacija.q_amp_par = q.parameter
left join(
select datum as h_datum, postaja, parameter, vred as h_vred, status_kolomon as h_kolomon from amp.podatki_zadnji
) h
on postaje.amp_sif = h.postaja and
lokacija.h_amp_par = h.parameter
left join(
select datum as t_datum, postaja, parameter, vred as t_vred, status_kolomon as tv_kolomon from amp.podatki_zadnji
) t
on postaje. amp_sif = t.postaja and
lokacija.tv_amp_par = t.parameter
/**
OTT stations
**/
union
/**
Drava Ptuj
**/
select kla_sif, amp_sif, part_ime, part_vodotok, wgs84_dolzina, wgs84_sirina, q_amp_par, q_scheme, q_table, q_datum, q_vred, h_amp_par, h_scheme, h_table, h_datum, h_vred, tv_amp_par, t_scheme, t_table, t_datum, t_vred, q_kolomon, h_kolomon, tv_kolomon from(
select kla_sif, amp_sif, part_ime, part_vodotok, wgs84_dolzina, wgs84_sirina from merilna_mesta where oea_id_oe=4 and amp_sif  in('H50')) postaje
left join(
select amp_sif as amp_sifra, q_scheme, q_table, q_amp_par, h_scheme, h_table, h_amp_par, t_scheme, t_table, tv_amp_par  from vode_pro.vw_postaje_par_in_podatki
) lokacija
on postaje.amp_sif=lokacija.amp_sifra
left join(
/*
--this doesn't work if one of nine cell speeds is malformed - function amp.F_OTT_RDI that calculates discharge from nine cell speed return null if one of the speeds is malformed (this happens very often)
select postaja, datum as q_datum, amp.f_ott_rdi('H50', (select datum from amp.podatki_zadnji where postaja = 'H50' and parameter = (select h_amp_par from mv_postaje_par_in_podatki where  amp_sif='H50'))).Q as q_vred
from amp.podatki_zadnji where postaja = 'H50' and parameter = (select h_amp_par from mv_postaje_par_in_podatki where  amp_sif='H50')
*/
--alter query 
select 'H50' postaja, o_get_np_datum as q_datum, o_get_np_vrednost as q_vred, o_get_np_status_kolomon as q_kolomon    
 from
    (SELECT * FROM TABLE (PKG_NP.F_GET_NP_VELICINA 
        (4
        ,(select kla_sif from merilna_mesta where oea_id_oe=4 and amp_sif='H50')
        , 'Q'
        , sysdate - 3/24
        ,  sysdate + 2/24
        , 0
        , 1))
        ORDER BY o_get_np_datum DESC)
        sub WHERE rownum = 1
) q
on postaje. amp_sif = q.postaja
left join(
select datum as h_datum, postaja, parameter, vred as h_vred, status_kolomon as h_kolomon from amp.podatki_zadnji
) h
on postaje.amp_sif = h.postaja and
lokacija.h_amp_par = h.parameter
left join(
select datum as t_datum, postaja, parameter, vred as t_vred, status_kolomon as tv_kolomon from amp.podatki_zadnji
) t
on postaje. amp_sif = t.postaja and
lokacija.tv_amp_par = t.parameter

union

/**
Log pod Mangrtom - kanal Roje
**/
select kla_sif, amp_sif, part_ime, part_vodotok, wgs84_dolzina, wgs84_sirina, q_amp_par, q_scheme, q_table, q_datum, q_vred, h_amp_par, h_scheme, h_table, h_datum, h_vred, tv_amp_par, t_scheme, t_table, t_datum, t_vred, q_kolomon, h_kolomon, tv_kolomon from(
select kla_sif, amp_sif, part_ime, part_vodotok, wgs84_dolzina, wgs84_sirina from merilna_mesta where oea_id_oe=4 and amp_sif  in('H52')) postaje
left join(
select amp_sif as amp_sifra, q_scheme, q_table, q_amp_par, h_scheme, h_table, h_amp_par, t_scheme, t_table, tv_amp_par from vode_pro.vw_postaje_par_in_podatki
) lokacija
on postaje.amp_sif=lokacija.amp_sifra
left join(
/*
--this doesn't work if one of nine cell speeds is malformed - function amp.F_OTT_RDI that calculates discharge from nine cell speed return null if one of the speeds is malformed (this happens very often)
select postaja, datum as q_datum, amp.f_ott_rdi('H52', (select datum from amp.podatki_zadnji where postaja = 'H52' and parameter = (select h_amp_par from mv_postaje_par_in_podatki where  amp_sif='H52'))).Q as q_vred
from amp.podatki_zadnji where postaja = 'H52' and parameter = (select h_amp_par from mv_postaje_par_in_podatki where  amp_sif='H52')
*/
--alter query 
select 'H52' postaja, o_get_np_datum as q_datum, o_get_np_vrednost as q_vred, o_get_np_status_kolomon as q_kolomon      
 from
    (SELECT * FROM TABLE (PKG_NP.F_GET_NP_VELICINA 
        (4
        ,(select kla_sif from merilna_mesta where oea_id_oe=4 and amp_sif='H52')
        , 'Q'
        , sysdate - 3/24
        ,  sysdate + 2/24
        , 0
        , 1))
        ORDER BY o_get_np_datum DESC)
        sub WHERE rownum = 1
) q
on postaje. amp_sif = q.postaja
left join(
select datum as h_datum, postaja, parameter, vred as h_vred, status_kolomon as h_kolomon from amp.podatki_zadnji
) h
on postaje.amp_sif = h.postaja and
lokacija.h_amp_par = h.parameter
left join(
select datum as t_datum, postaja, parameter, vred as t_vred, status_kolomon as tv_kolomon from amp.podatki_zadnji
) t
on postaje. amp_sif = t.postaja and
lokacija.tv_amp_par = t.parameter

union

/**
Drava Črneče
**/
select kla_sif, amp_sif, part_ime, part_vodotok, wgs84_dolzina, wgs84_sirina, q_amp_par, q_scheme, q_table, q_datum, q_vred, h_amp_par, h_scheme, h_table, h_datum, h_vred, tv_amp_par, t_scheme, t_table, t_datum, t_vred, q_kolomon, h_kolomon, tv_kolomon from(
select kla_sif, amp_sif, part_ime, part_vodotok, wgs84_dolzina, wgs84_sirina from merilna_mesta where oea_id_oe=4 and amp_sif  in('H58')) postaje
left join(
select amp_sif as amp_sifra, q_scheme, q_table, q_amp_par, h_scheme, h_table, h_amp_par, t_scheme, t_table, tv_amp_par from vode_pro.vw_postaje_par_in_podatki
) lokacija
on postaje.amp_sif=lokacija.amp_sifra
left join(
/*
--this doesn't work if one of nine cell speeds is malformed - function amp.F_OTT_RDI that calculates discharge from nine cell speed return null if one of the speeds is malformed (this happens very often)
select postaja, datum as q_datum, amp.f_ott_rdi('H58', (select datum from amp.podatki_zadnji where postaja = 'H58' and parameter = (select h_amp_par from mv_postaje_par_in_podatki where  amp_sif='H58'))).Q as q_vred
from amp.podatki_zadnji where postaja = 'H58' and parameter = (select h_amp_par from mv_postaje_par_in_podatki where  amp_sif='H58')
*/
--alter query 
 select 'H58' postaja, o_get_np_datum as q_datum, o_get_np_vrednost as q_vred, o_get_np_status_kolomon as q_kolomon      
 from
    (SELECT * FROM TABLE (PKG_NP.F_GET_NP_VELICINA 
        (4
        ,(select kla_sif from merilna_mesta where oea_id_oe=4 and amp_sif='H58')
        , 'Q'
        , sysdate - 3/24
        ,  sysdate + 2/24
        , 0
        , 1))
        ORDER BY o_get_np_datum DESC)
        sub WHERE rownum = 1
) q
on postaje. amp_sif = q.postaja
left join(
select datum as h_datum, postaja, parameter, vred as h_vred, status_kolomon as h_kolomon from amp.podatki_zadnji
) h
on postaje.amp_sif = h.postaja and
lokacija.h_amp_par = h.parameter
left join(
select datum as t_datum, postaja, parameter, vred as t_vred, status_kolomon as tv_kolomon from amp.podatki_zadnji
) t
on postaje. amp_sif = t.postaja and
lokacija.tv_amp_par = t.parameter

union

/**
Sava Dolinka - Jesenice
**/
select kla_sif, amp_sif, part_ime, part_vodotok, wgs84_dolzina, wgs84_sirina, q_amp_par, q_scheme, q_table, q_datum, q_vred, h_amp_par, h_scheme, h_table, h_datum, h_vred, tv_amp_par, t_scheme, t_table, t_datum, t_vred, q_kolomon, h_kolomon, tv_kolomon from(
select kla_sif, amp_sif, part_ime, part_vodotok, wgs84_dolzina, wgs84_sirina from merilna_mesta where oea_id_oe=4 and amp_sif  in('H439')) postaje
left join(
select amp_sif as amp_sifra, q_scheme, q_table, q_amp_par, h_scheme, h_table, h_amp_par, t_scheme, t_table, tv_amp_par  from vode_pro.vw_postaje_par_in_podatki
) lokacija
on postaje.amp_sif=lokacija.amp_sifra
left join(
/*
--this doesn't work if one of nine cell speeds is malformed - function amp.F_OTT_RDI that calculates discharge from nine cell speed return null if one of the speeds is malformed (this happens very often)
select postaja, datum as q_datum, amp.f_ott_rdi('H50', (select datum from amp.podatki_zadnji where postaja = 'H50' and parameter = (select h_amp_par from mv_postaje_par_in_podatki where  amp_sif='H50'))).Q as q_vred
from amp.podatki_zadnji where postaja = 'H50' and parameter = (select h_amp_par from mv_postaje_par_in_podatki where  amp_sif='H50')
*/
--alter query 
select 'H439' postaja, o_get_np_datum as q_datum, o_get_np_vrednost as q_vred, o_get_np_status_kolomon as q_kolomon      
 from
    (SELECT * FROM TABLE (PKG_NP.F_GET_NP_VELICINA 
        (4
        ,(select kla_sif from merilna_mesta where oea_id_oe=4 and amp_sif='H439')
        , 'Q'
        , sysdate - 3/24
        ,  sysdate + 2/24
        , 0
        , 1))
        ORDER BY o_get_np_datum DESC)
        sub WHERE rownum = 1
) q
on postaje. amp_sif = q.postaja
left join(
select datum as h_datum, postaja, parameter, vred as h_vred, status_kolomon as h_kolomon from amp.podatki_zadnji
) h
on postaje.amp_sif = h.postaja and
lokacija.h_amp_par = h.parameter
left join(
select datum as t_datum, postaja, parameter, vred as t_vred, status_kolomon as tv_kolomon from amp.podatki_zadnji
) t
on postaje. amp_sif = t.postaja and
lokacija.tv_amp_par = t.parameter
/*
END OD STATION QUERY
*/
) podatki
/*
JOIN WARNING LEVEL FOR DISCHARGE
*/
left join (
select hid_sif, susni_pretok, mali_pretok, obicajni_pretok, velik_pretok, rumeni_alarm, oranzni_alarm, rdeci_alarm from vode_pro.mv_znacilni_pretoki
) opozorilni
on podatki.kla_sif = opozorilni.hid_sif
/*
JOIN WARNING LEVEL FOR WATER LEVEL
*/
left join (
select hidr, shnp, shsr, shvp, susni_h, mali_h, obicajni_h, veliki_h, rumeni_h, oranzni_h, rdeci_h from(
select kla_sif as hidr from merilna_mesta where oea_id_oe = 4) post
left join(
select kla_sif as sif, shnp, shsr, shvp from vw_obdobne_h
)h_obd
on post.hidr = h_obd.sif
left join(
select kla_sif, susni_h, mali_h, obicajni_h, veliki_h, rumeni_h, oranzni_h, rdeci_h from vw_znacilni_vodostaji
) h_znac
on post.hidr = h_znac.kla_sif
) h_znacilni
on podatki.kla_sif = h_znacilni.hidr

/*
warning_region
*/
left join (
select 
h.station_id, 
h.ha_id ,
r.ha_name
from 
station_warning_region h,
hidroalarm_regions r
where h.ha_id=r.ha_id
) ha_regije
on podatki.kla_sif = ha_regije.station_id
/***IP KAMERE***/
left join(
select kla_sif as kl, sif_kamere as ip_kamere from vode_pro.hidroloske_kamere where aktivna='T'
) kamera
on podatki.kla_sif = kamera.kl
/**
JOIN TENDENCA by Marjan
**/
left join(

select sif, zadnja_meritev, max(tendenca) as tendenca from (
select kla_sif as sif, zadnja_meritev, tendenca from(
select kla_sif from merilna_mesta where oea_id_oe=4) osnova
left join(
select a.kla_sif as sif, a.zadnja_meritev, a.tendenca
from vode_pro.tendence a where a.zadnja_meritev = (select max(b.zadnja_meritev) from vode_pro.tendence b where a.kla_sif = b.kla_sif)
) tend
on osnova.kla_sif = tend.sif
) group by sif, zadnja_meritev

) tendence
on podatki.kla_sif = tendence.sif
/*
left join(
select kla_sif as sif, zadnja_meritev, tendenca from(
select kla_sif, max(zadnja_meritev) as zadnja_meritev from tendence group by kla_sif) datum
left join(
select kla_sif as sif, zadnja_meritev as mer, tendenca from tendence
) tendenca
on tendenca.mer= datum.zadnja_meritev
and tendenca.sif = datum.kla_sif
)tend 
on podatki.kla_sif = tend.sif
*/
/*
NASTAVITVE - is it active
*/
left join(
select hidroloska, aktivna, vodilna_kolicina from vw_postaje_nastavitve
) nast
on nast.hidroloska = podatki.kla_sif
/*
ADD field if param H, Q, Tv is active
*/
left join(
select hid_sif, h_id, q_id, tv_id from vodepro_postaje_nastavitve
) active_p
on active_p.hid_sif = podatki.kla_sif) main_data
/*
ADD DESCRIPTION OF ZNACILNA VREDNOST --> check speed, before adding this view took about 0,5 to 0,75 seconds
*/
left join(
select id_znacilne, opis_znacilne from znacilne_vrednosti_sifrant
)znacilne
on main_data.q_znacilna_vrednost = znacilne.id_znacilne;


```