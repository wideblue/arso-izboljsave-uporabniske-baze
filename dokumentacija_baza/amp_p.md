#### AMP_P

V tabelo so vpisani podatki na

Struktura tabele je:

|  Column  |            Type             | Modifiers |
|:-------: |:---------------------------:| :--------:|
| idmm     | smallint                    |           |  
| tip      | smallint                    |           |           
| datum    | date                        |           |
| cas      | time without time zone      |           |
| r        | real                        |           |
| r1       | real                        |           |
| r2       | real                        |           |
| r3       | real                        |           |
| r4       | real                        |           |
| r5       | real                        |           |
| r6       | real                        |           |
| rr       | real                        |           |
| status   | smallint                    |           |
| statusi  | real[]                      |           |
| par      | smallint                    |           |
| datum_cas| timestamp without time zone |           |

Indexes:
    "amp_p_idx" UNIQUE, btree (idmm, tip, datum, cas, par, datum_cas)
    "amp_p_cas" btree (cas)
    "amp_p_datum" btree (datum)
    "amp_p_datum_cas" btree (datum_cas)

Stolpec statusi je vektorsko zapisan. Na 4. mestu (statusi[4]) je zapisana [veljavnost podatka](http://tmpvirga.rzs-hm.si/~mihad/kontrola_bobrovih_indeksov/opis_kontrol.pdf), ki ga beleži sama postaja. Na 7. mestu (statusi[7]) je zapisan dele pravilno izmerjenih podatkov. Za analizo veljavnosti podatkov klikni [tukaj](http://tmpvirga.rzs-hm.si/~mihad/kontrola_bobrovih_indeksov/analiza_statusov.php).

Parametri, ki se nahajajo v tabeli amp_p so naslednji:

| parameter | opis                                                        |
|:--------: | :---------------------------------------------------------- |
| 7	        | padavine - količina padavin [mm],tehtalni ombrometer Pluvio2 |
| 3120	    | padavine - količina padavin [mm],tehtalni ombrometer Pluvio2 |
| 3125	    | optično izmerjena količina padavin [mm]                       |
| 3126	    | sedanje vreme SYNOP (WMO 4680)                              |
| 3129	    | trajanje padavin [min]                                      |
| 3150	    | skupna višina snežne odeje [cm]                               |
| 3151	    | višina snega-jakost signala (signal strength)                |
| 3304	    | trajanje sončnega obsevanja v desetinah ure ali [min]        |

Pri parametrih nad 3000 se jih programsko agregira skupaj. Same postaje pri teh parametrih imajo izpis na 10 minut. V tej tabeli so dodani predvsem zato, da so vsi podatki padavin zbrani v isti tabeli. Izjemi sta tudi 2 Ottovi postaji, in sicer H36 Dravijna Loce in H41 Ljubljanica Moste, ki imata v resnici podatke merjene na 10 minut in ne na 5 minut. Pri teh dveh postajah se pri vpisu 5 minutnih padavin 10 minutna koliina padavin razdeli na 2 enaka dela.

Za prepis podatkov iz Tajfuna (tabela podatki_padavine, podatki_ott in podatki_k_1) na tmpvirgo v tabelo amp_p skrbi program:
* /home/mihad/programi/oracle/amp_p/prenos_oracle_amp_p.pl, časi prepisa so vsako uro ob 3,14,25,34,44,55 minuti
* /home/mihad/programi/oracle/amp_p/podatki_padavine.pl, časi prepisa so vsako uro ob 4,13,34,43 minuti
* /home/mihad/programi/oracle_10min/amp_p/prepis_v_amp_p.pl, časi prepisa so vsako uro ob 8,11,38,41 minuti

