#### AMP_O

V tabeli so shranjeni tisti podatki samodejnih postaj, ki beležijo terminsko vrednost, povprečje, maksimum in minimum parametra. Izjema je veter, ki se vpisuje v svojo tabelo. Parametri, ki so na voljo so na voljo v tabeli:

Struktura tabele:
                   Table "public.amp_o"
                   
|     Column      |            Type             | Modifiers |
| :-------------: | :-------------------------: | :-------: |
|      idmm       |           integer           |           |
|       tip       |          smallint           |           |
|      datum      |            date             |           |
|       cas       |   time without time zone    |           |
|       par       |          smallint           |           |
|      vred       |            real             |           |
|       pov       |            real             |           |
|      maxv       |            real             |           |
|      tmax       |   time without time zone    |           |
|      minv       |            real             |           |
|      tmin       |   time without time zone    |           |
|       std       |            real             |           |
|     status      |          smallint           |           |
|     statusi     |           real[]            |           |
|    datum_cas    | timestamp without time zone |           |
| status_senzorja |            text             |           |

Indexes:
    "amp_o_idx" UNIQUE, btree (idmm, tip, datum, cas, par, datum_cas)
    "amp_o_cas" btree (cas)
    "amp_o_datum_cas" btree (datum_cas)
    "amp_o_datum_idx" btree (datum)


Parametri, ki se nahajajo v tabeli amp_o so naslednji:

| par  | opis                                                                                                                                                  |
| ---- | ----------------------------------------------------------------------------------------------------------------------------------------------------- |
| 1    | temperatura zraka 2m nad tlemi [oC] - neaspirirana v meteoroloski hišici                                                                              |
| 2    | nereduciran zračni tlak na postaji [hPa]                                                                                                              |
| 3    | relativna vlažnost zraka 2m nad tlemi [odstotki]                                                                                                      |
| 4    | globalno sončno obsevanje[W/m2]primarni piranometer Kipp & Zonen SMP 11, ali globalno sončno obsevanje[W/m2] kombinirani solarni senzor Delta-T SPN 1 |
| 5    | difuzno sončno obsevanje [W/m2]                                                                                                                       |
| 8    | temperatura na 0.5m (50cm)                                                                                                                            |
| 9    | temperatura na 0.05m (5cm) nad tlemi [oC]                                                                                                             |
| 11   | temperatura zemlje v globini 0.05m (5cm)                                                                                                              |
| 12   | temperatura zemlje v globini 0.10m (10cm)                                                                                                             |
| 13   | temperatura zemlje v globini 0.30m (30cm)                                                                                                             |
| 14   | vlažnost lista (0 ali 1),trajanje omočenosti listja [min]                                                                                             |
| 15   | stabilnost atmosfere - NEK                                                                                                                            |
| 16   | temperatura na 10m (stolp NEK)                                                                                                                        |
| 19   | relativna vlaga na 10m (stolp NEK)                                                                                                                    |
| 26   | UV eritermalno sevanje [MED/h]                                                                                                                        |
| 27   | temperatura radiometra za UVB sevanje[oC]                                                                                                             |
| 29   | dolgovalovno sevanje navzdol (P48)                                                                                                                    |
| 30   | temperatura senzorja za P48                                                                                                                           |
| 35   | Horizontalna vidnost RVR31,vidnost vzdolz vzletno-pristajalne steze RVR (runway visual range) [m] RWY / TDZ                                           |
| 36   | Horizontalna vidnost RVRMID,vidnost vzdolz vzletno-pristajalne steze RVR (runway visual range) [m] RWY / MID                                          |
| 37   | Horizontalna vidnost RVR13,vidnost vzdolz vzletno-pristajalne steze RVR (runway visual range) [m] RWY / END                                           |
| 38   | aeronavtična vidnost VIS (aeronautical visibility) [m] RWY / TDZ                                                                                      |
| 39   | aeronavtična vidnost VIS (aeronautical visibility) [m] RWY / MID                                                                                      |
| 40   | aeronavtična vidnost VIS (aeronautical visibility) [m] RWY / END                                                                                      |
| 41   | baza oblakov na pragu 31                                                                                                                              |
| 42   | baza oblakov na pragu 13                                                                                                                              |
| 43   | intenziteta signalnih luči na vzletno-pristajalni stezi [odstotki]samo letališči Brnik in Cerklje ob Krki                                             |
| 44   | redundantni nereduciran zračni tlak na postaji [hPa] - samo na letaliscih                                                                             |
| 45   | svetlost ozadja na letališču(background luminance) [cd/m2]                                                                                            |
| 48   | temperatura vode ali morja (2m globine)                                                                                                               |
| 71   | temperatura merjena izven hisice v zaklonu ali z redundancnim senzorjem                                                                               |
| 72   | vlaga merjena izven hisice v zaklonu ali z redundancnim senzorjem                                                                                     |
| 76   | horizontalna meteorološka optična vidnost MOR (meteorological optical range) [m] na sinoptičnih postajah AMWS I in II                                 |
| 77   | temperatura zraka 5cm (0.05m) nad tlemi [°C] - brez radiacijskega zaklona nad golim zemljiščem (EIGENBRODT - LTS 2000,MB-letališče,MS)                |
| 3005 | notranja temperatura avtomatske postaje [oC]                                                                                                          |
| 3010 | temperatura zraka 2m nad tlemi [°C] - neaspirirana v meteorološki hisici                                                                              |
| 3011 | redundantna temperatura zraka 2m nad tlemi [oC] - neaspirirana v meteorološki hišici                                                                  |
| 3040 | temperatura zraka 5cm (0.05m) nad tlemi [°C] - brez radiacijskega zaklona nad golim zemljišščem                                                       |
| 3045 | relativna vlažnost zraka 2m nad tlemi [odstotki]                                                                                                      |
| 3046 | temperatura mokrega termometra [°C] wet bulb temperature                                                                                              |
| 3047 | temperatura rosišča [oC] dewpoint temperature                                                                                                         |
| 3050 | redundantna relativna vlažnost zraka 2m nad tlemi                                                                                                     |
| 3051 | redundantna temperatura mokrega termometra [oC] wet bulb temperature                                                                                  |
| 3052 | redundantna temperatura rosišča [oC] dewpoint temperature                                                                                             |
| 3090 | nereduciran zračni tlak na postaji [hPa]                                                                                                              |
| 3091 | drseca 3 urna tendenca [hPa] in oblika krivulje tlaka po WMO kodni tabeli stev. 0200 - vse postaje z barometri Vaisala PTB                            |
| 3092 | oblika krivulje tlaka po WMO kodni tabeli štev. 0200 - vse postaje z barometri Vaisala PTB                                                            |
| 3093 | redundantni nereduciran zračni tlak na postaji [hPa] - samo na letaliscih                                                                             |
| 3094 | redundantni-drseča 3 urna tendenca [hPa] in oblika krivulje tlaka po WMO kodni tabeli stev. 0200 - vse postaje z barometri Vaisala PTB                |
| 3095 | redundantni-oblika krivulje tlaka po WMO kodni tabeli štev. 0200 - vse postaje z barometri Vaisala PTB                                                |
| 3130 | sedanje vreme - sinop koda                                                                                                                            |
| 3131 | optično izmerjena intenziteta padavin                                                                                                                 |
| 3210 | horizontalna meteorološka optična vidnost MOR (meteorological optical range) [m] na sinoptičnih postajah AMWS I in II                                 |
| 3211 | ekstinkcijski koeficient TOTAL EXCO [/km]                                                                                                             |
| 3213 | Horizontalna vidnost RVR31,vidnost vzdolz vzletno-pristajalne steze RVR (runway visual range) [m] RWY / TDZ                                           |
| 3214 | Horizontalna vidnost RVRMID,vidnost vzdolž vzletno-pristajalne steze RVR (runway visual range) [m] RWY / MID                                          |
| 3215 | Horizontalna vidnost RVR13,vidnost vzdolž vzletno-pristajalne steze RVR (runway visual range) [m] RWY / END                                           |
| 3219 | (horizontalna)meteorološka optična vidnost MOR (meteorological optical range) [m] RWY / TDZ                                                           |
| 3220 | (horizontalna)meteorološka optična vidnost MOR (meteorological optical range) [m] RWY / MID                                                           |
| 3221 | (horizontalna)meteorološka optična vidnost MOR (meteorological optical range) [m] RWY / END                                                           |
| 3222 | intenziteta signalnih luči na vzletno-pristajalni stezi [odstotki]samo letališči Brnik in Cerklje ob Krki                                             |
| 3223 | svetlost ozadja na letališču(background luminance) [cd/m2]                                                                                            |
| 3224 | izračunana prevladujoča vidnost iz vseh treh optičnih merilnikov PV (prevailing visibility) [m] samo letališči Brnik in Cerklje ob Krki               |
| 3251 | količina prvega oblačnega sloja 0do9 [N/8] (Brnik na R30 TZD, do 8_2017 R31)                                                                          |
| 3252 | višina baze prvega oblačnega sloja [ft]                                                                                                               |
| 3253 | višina baze prvega oblačnega sloja [m]                                                                                                                |
| 3254 | količina drugega oblačnega sloja 0 do 9 [N/8]                                                                                                         |
| 3255 | višina baze drugega oblačnega sloja [ft]                                                                                                              |
| 3256 | višina baze drugega oblačnega sloja [m]                                                                                                               |
| 3257 | količina tretjega oblačnega sloja 0 do 9 [N/8]                                                                                                        |
| 3258 | višina baze tretjega oblačnega sloja [ft]                                                                                                             |
| 3259 | višina baze tretjega oblačnega sloja [m]                                                                                                              |
| 3260 | količina četrtega oblačnega sloja 0 do 9 [N/8]                                                                                                        |
| 3261 | višina baze četrtega oblačnega sloja [ft]                                                                                                             |
| 3262 | višina baze četrtega oblačnega sloja [m]                                                                                                              |
| 3264 | količina prvega oblačnega sloja 0 do 9 [N/8]                                                                                                          |
| 3265 | višina baze prvega oblačnega sloja [ft]                                                                                                               |
| 3266 | višina baze prvega oblačnega sloja [m]                                                                                                                |
| 3267 | količina drugega oblačnega sloja 0 do 9 [N/8]                                                                                                         |
| 3268 | višina baze drugega oblačnega sloja [ft]                                                                                                              |
| 3269 | višina baze drugega oblačnega sloja [m]                                                                                                               |
| 3270 | količina tretjega oblačnega sloja 0 do 9 [N/8]                                                                                                        |
| 3271 | višina baze tretjega oblačnega sloja [ft]                                                                                                             |
| 3272 | višina baze tretjega oblačnega sloja [m]                                                                                                              |
| 3273 | količina četrtega oblačnega sloja 0 do 9 [N/8]                                                                                                        |
| 3274 | višina baze četrtega oblačnega sloja [ft]                                                                                                             |
| 3275 | višina baze četrtega oblačnega sloja [m]                                                                                                              |
| 3300 | globalno sončno obsevanje[W/m2]primarni piranometer Kipp & Zonen SMP 11, ali globalno sončno obsevanje[W/m2] kombinirani solarni senzor Delta-T SPN 1 |
| 3302 | globalno sončno obsevanje [W/m2] kombinirani solarni senzor Delta-T SPN 1                                                                             |
| 3303 | difuzno sončno obsevanje [W/m2]                                                                                                                       |
| 3309 | temperatura radiometra za eritermalno sevanje [°C]                                                                                                    |
| 3310 | UV-B sevanje [mW/m2]                                                                                                                                  |
| 3312 | dolgovalovno IR sevanje                                                                                                                               |
| 3313 | temperatura senzorja za dolgovalovno IR                                                                                                               |
| 3314 | napajalna napetost za dolgovalovno IR                                                                                                                 |
| 3370 | temperatura zemlje v globini 0.05m (5cm)                                                                                                              |
| 3371 | temperatura zemlje v globini 0.10m (10cm)                                                                                                             |
| 3372 | temperatura zemlje v globini 0.2m (20cm)                                                                                                              |
| 3373 | temperatura zemlje v globini 0.30m (30cm)                                                                                                             |
| 3374 | temperatura zemlje v globini 0.5m (50cm)                                                                                                              |
| 3390 | vlažnost tal v 1. sloju [odstotki] na 10cm                                                                                                            |
| 3391 | temperatura v 1. sloju [oC] na 10cm                                                                                                                   |
| 3392 | električna prevodnost tal v 1. sloju [dS/m] na 10cm                                                                                                   |
| 3393 | vlažnost tal v 2. sloju [odstotki] na 20cm                                                                                                            |
| 3394 | temperatura v 2. sloju [oC] na 20cm                                                                                                                   |
| 3395 | električna prevodnost tal v 2. sloju [dS/m] na 20cm                                                                                                   |
| 3396 | vlažnost tal v 3. sloju [odstotki] na 30cm                                                                                                            |
| 3397 | temperatura v 3. sloju [oC] na 30cm                                                                                                                   |
| 3398 | električna prevodnost tal v 3. sloju [dS/m] na 30cm                                                                                                   |
| 3480 | temperatura zraka v komunikacijski omarici(kontejner)                                                                                                 |
| 3481 | relativna vlaga v komunikacijski omarici(kontejner                                                                                                    |
| 3482 | napajalna napetost v komunikacijski omarici(kontejner)                                                                                                |


Za prepis podatkov iz Tajfuna (tabele podatki, podatki_ott in podatki_k) na tmpvirgo v tabelo amp_o skrbi več različnih skript, ki se nahajajo na naslednjih naslovih:
* /home/mihad/programi/oracle/amp_o/crpanje_tabela_podatki.pl , časi prepisa so vsako uro ob 7,13,14,20,37,42,53 minuti
* /home/mihad/programi/oracle/amp_o/podatki_T_rv_ott.pl , časi prepisa so vsako uro ob 5,11,35,41 minuti
* /home/mihad/programi/oracle_10min/amp_o/crpanje_tabela_podatek.pl, časi prepisa so vsako uro ob 9,14,39,44 minuti

