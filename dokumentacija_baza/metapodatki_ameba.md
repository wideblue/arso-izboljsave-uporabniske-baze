# METAPODATKI NA AMEBI

Na Amebi se kot osnova za določanje metapodatkov uporabljajo 3 tabele:


##  AMEBA.IDMM

Primary key je IDMM. Tabela vsebuje podatke o lokaciji za vse meteoroloških postajah, ki imajo dodeljen idmm.

Struktura tabele:

| COLUMN_NAME        | DATA_TYPE        | DESCRIPTION |
|--------------------|------------------|-------------|
| idmm               | integer          |             |
| geometry           | geometry         |             |
| st_postaje         | integer          |             |
| ime_postaje        | character        |             |
| nadm_visina        | double precision |             |
| ge_sirina          | double precision |             |
| ge_dolzina         | double precision |             |
| ime_vnasalca       | character        |             |
| datum_vnosa        | timestamp        |             |
| icon               | text             |             |
| label              | text             |             |
| ime_postaje_objava | text             |             |
| ge_sir_wgs84       | double precision |             |
| ge_dol_wgs84       | double precision |             |



### PRAVILO ZA SPREMEMBO IDMM

Kriteriji za določanje IDMM -ja:

IDMM ostane enak, če so spremembe lokacije meritev na:

1.) glavni postaji znotraj opazovalnega prostora do 20m.

2.) klimatološka postaja: do oddaljenosti  10m,  višinske razlike  5m.

3.) padavinska postaja: do oddaljenosti 5m,   višinske razlike 3m.


Primer: 

I.) padavinski postaji Poddraga se je dežmer prestavil za cca. 40 m, treba je narediti nov IDMM.

II.) cca. 40 m od klimatološke postaje je na novo postavljena bobrova samodejna postaja. Samodejna postaja ima drug idmm kot klimatološka.

To pravilo je sedaj tudi vključeno v navodila za urejanje metapodatkov:
http://tmpvirga.rzs-hm.si/~mihad/sprememba_metapodatkov/NAVODILA_ZA_SPREMINJANJE_METAPODATKOV.doc  (30.05.2019)



## AMEBA.ID_OPS

Tabela vsebuje podatke o o vrstah postaj, ki merijo na posamezni lokaciji.
Primer: na eni lokaciji lahko izvajamo meritve s klasično in bobrovo postajo. lokacija ima en idmm, vsaka postaja ima pa svoj id. V primeru vzporednih meritev lahko prikažemo na isti lokaciji npr. dve meritvi temeperature.

Shema tabele:

| COLUMN_NAME             | DATA_TYPE | DESCRIPTION |
|-------------------------|-----------|-------------|
| id                      | integer   |             |
| idmm                    | integer   |             |
| tip                     | smallint  |             |
| sifra_meritve           | character |             |
| datum_zacetka           | timestamp |             |
| datum_konca             | timestamp |             |
| ime_vnasalca            | character |             |
| datum_vnosa             | timestamp |             |
| sifra_meritve_stare_amp | text      |             |
| datum_zacetka_stare_amp | timestamp |             |
| datum_konca_stare_amp   | timestamp |             |
| datum_zacetka_bober_amp | timestamp |             |
| datum_konca_bober_amp   | timestamp |             |



Tabela ID_OPS se naveže na tabelo AMEBA.IDMM preko idmm in na tabelo AMEBA.PARAMETRI preko id.

## AMEBA.PARAMETRI

Tabela vsebuje podatke  o vseh parametrih, ki se merijo na posamezni postaji.

Shema tabele:

| COLUMN_NAME       | DATA_TYPE | DESCRIPTION |
|-------------------|-----------|-------------|
| id                | smallint  |             |
| id_parametra      | smallint  |             |
| datum_zacetka     | date      |             |
| datum_konca       | date      |             |
| id_parametra_ismm | smallint  |             |
| id_naprave_ismm   | smallint  |             |
| id_velicine_ismm  | smallint  |             |










#### OPOMBA: Zapisano bo pregledal in dopolnil Miha Demšar.