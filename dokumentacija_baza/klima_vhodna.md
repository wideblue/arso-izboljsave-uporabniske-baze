#### KLIMA_VHODNA

V tabeli klima_vhodna so vpisani podatki iz klimatoloških (tip=2) in glavnih postaj (tip=3). Opazovalci na glavnih postajah vpišejo podatek za pretekli dan do 9 UTC tekočega dne. Podatke napisane v klimatološkem dnevniku za pretekli mesece opazovalci iz klimatoloških postaj po pošti pošljejo v začetku meseca. V postopku zajema (v Celju, Murski Soboti, letališču Maribor, Bilju in na letališču J. Pučnika) se podatke nato čimprej vpiše. Podatke se nato v postopku kontrole v najkrajšem možnem času skontrolira.

Več o tem je bilo napisano [tukaj] (http://venera.arso.sigov.si/Meteo/Kontrola/dokument/parametri_stev_pretvorbe.html).


Struktura tabele:

|             Column              |            Type             |                          Modifiers
|---------------------------------|:---------------------------:|:-------------------------------------------------------------
| postaja                         | smallint                    |
| leto                            | smallint                    |
| mesec                           | smallint                    |
| dan                             | smallint                    |
| k5_pritisk_7h                   | smallint                    |
| k6_pritisk_14h                  | smallint                    |
| k7_pritisk_21h                  | smallint                    |
| k8_maksimalna_temperatura       | smallint                    |
| k9_minimalna_temperatura        | smallint                    |
| k10_minimalna_temperatura_5cm   | smallint                    |
| k11_temperatura_suhi_7h         | smallint                    |
| k12_temperatura_suhi_14h        | smallint                    |
| k13_temperatura_suhi_21h        | smallint                    |
| k14_temperatura_mokri_7h        | smallint                    |
| k15_temperatura_mokri_14h       | smallint                    |
| k16_temperatura_mokri_21h       | smallint                    |
| k17_led_7h                      | smallint                    |
| k18_led_14h                     | smallint                    |
| k19_led_21h                     | smallint                    |
| k20_rel_vlaga_7h                | smallint                    |
| k21_rel_vlaga_14h               | smallint                    |
| k22_rel_vlaga_21h               | smallint                    |
| k23_inter_tmaks                 | smallint                    |
| k24_inter_tmin                  | smallint                    |
| k25_inter_tmin5                 | smallint                    |
| k26_inter_ts07                  | smallint                    |
| k27_inter_ts14                  | smallint                    |
| k28_inter_ts21                  | smallint                    |
| k29_inter_tm07                  | smallint                    |
| k30_inter_tm14                  | smallint                    |
| k31_inter_tm21                  | smallint                    |
| k32_inter_padavine              | smallint                    |
| k33_smer_vetra_7h               | smallint                    |
| k34_hitrost_vetra_7h            | smallint                    |
| k35_smer_vetra_14h              | smallint                    |
| k36_hitrost_vetra_14h           | smallint                    |
| k37_smer_vetra_21h              | smallint                    |
| k38_hitrost_vetra_21h           | smallint                    |
| k39_stanje_tal_7h               | smallint                    |
| k40_stanje_tal_14h              | smallint                    |
| k41_stanje_tal_21h              | smallint                    |
| k42_vidnost_7h                  | smallint                    |
| k43_vidnost_14h                 | smallint                    |
| k44_vidnost_21h                 | smallint                    |
| k45_trajanje_sonca              | smallint                    |
| k46_oblacnost_7h                | smallint                    |
| k47_oblacnost_14h               | smallint                    |
| k48_oblacnost_21h               | smallint                    |
| k49_padavine                    | smallint                    |
| k50_oblika_padavin              | smallint                    |
| k51_sneg_skupaj                 | smallint                    |
| k52_sneg_novi                   | smallint                    |
| k53_voda_v_snegu                | smallint                    |
| k54_dez_rosenje_ploha_dezja     | smallint                    |
| k55_dez_zmrz_rosen_zmrz_iglice  | smallint                    |
| k56_sneg_zrnat_sneg_ploha_snega | smallint                    |
| k57_dez_s_sn_babje_psen_ploh_ds | smallint                    |
| k58_toca_sodra_dim              | smallint                    |
| k59_megla_megla_z_vid_neb_led_m | smallint                    |
| k60_meglic_suha_motnost_talna_m | smallint                    |
| k61_rosa_slana_prsec_z_vodne_p_ | smallint                    |
| k62_poledica_zled_poled__na_tl_ | smallint                    |
| k63_ivje_trdo_ivje_elijev_og_   | smallint                    |
| k64_nevihta_grmenje_bliskanje   | smallint                    |
| k65_mocan_veter_6bf_8bf         | smallint                    |
| k66_snezni_vrtici_tromba_1      | smallint                    |
| k67_prasni_pesceni_vrtinci_2    | smallint                    |
| k68_halo_venec_ok_sonca_mavrica | smallint                    |
| k69_halo_venec_ok_lune_zrcalje  | smallint                    |
| k70_snezna_odeja                | smallint                    |
| idmm                            | integer                     | not null
| tip                             | smallint                    | not null
| datum                           | date                        | not null
| cas_vnosa                       | timestamp without time zone | not null default ('now'::text)::timestamp(6) with time zone
| ime_vnasalca                    | character varying           |

Indexes:
    "klima_vhodna_datum_index" btree (datum)
    "klima_vhodna_idx" btree (idmm, datum)
    "klima_vhodna_postaja_index" btree (postaja)
    "postaja_lmd_datum_idmm_kkd_index" btree (postaja, leto, mesec, dan, datum, idmm)
