# Izboljšave uporabniške baze

Repozitorij, ki je namenjen spremljanju izboljšav uporabniških tabel in meta tabel v bazi Ameba.

### Dokumentacija

Dokumentacija ureditve trenutne baze je v direktoriju [dokumentacija_baza]. Dokumenti, ki naj bi bili podlaga za prenovo, pa se zbirajo v direktoriju [dokumenti-prenove].

### Dodajanje opisa težav

V stranskem meniju je povezava do [Issues], kjer se lahko doda opis posamezne težave. 

V podmeniju je povezava do [Boards], kjer bo na tablah prikazano, katere težave je potrebno odpraviti in katere težave se trenutno rešuje.


[Issues]: https://gitlab.com/wideblue/arso-izboljsave-uporabniske-baze/issues
[Boards]: https://gitlab.com/wideblue/arso-izboljsave-uporabniske-baze/boards
[dokumentacija_baza]: dokumentacija_baza
[dokumenti-prenove]: dokumenti-prenove