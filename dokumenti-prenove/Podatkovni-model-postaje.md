# Podatkovni model za podatke iz postaj

## Fizična realnost
Imamo senzor na določenem mestu:
* senzor se spremeni 
  - v senzor istega tipa
  - v drug tip senzorja
* spremeni lokacija
* obdobje merjenja
* na lokaciji imamo v istem času več senzorjev za isto veličino

Na istem mestu imamo lahko sledečo sliko nizov meritev čez neko obdobje

```
|----niz 1--------------| klasični termometer 
        |-----niz 2-----------| avtomatska postaja
        |-----niz 3-----------| redundančna meritev
                |-------niz 4-----| nova bober postaja
                                  |-----niz 5-----| merilni gumb
```        
   

Grupiranje senzorjev - Postaja
* spremeni lokacija
* nabor senzorjev
* več senzorjev za isti parameter (kateri je primarni)

## Podatkovni model v bazi

### Obstoječe stanje v meteorološki bazi Ameba

* številka postaje
* IDMM (ID merilnega mesta )
* tip postaje 
* par (id prametra, različen par za isto veličino glede na tip postaje)

### Predlog novega modela

Osnova za ureditev je časovni niz za določeno veličino na določeni lokaciji izmerjen z istim tipom inštrumenta. Lokacija je določena z merilnim mestom (IDMM) oziroma koordinatami parcele ne s točnimi koordinatami senzorja. Grupiranje časovih nizov poteka na nivoju merilnega mesta (parcela,  "postaj") in na širšem območju npr. občine (mesto, kraj) ID_občine. Potrebno je določiti pravila, kdaj se zamenja ID_občine in kdaj samo IDMM.
Nizi na isti lokaciji dobivajo zaporedno številko niza. 
Ta uredidev omogoča, da imamo na isti parceli več vzporednih meritev z različnimi ali enakimi senzorji, ter različnimi ali enakimi merilnimi postajami. Metapodatki o nizu so lahko v drugi tabeli "Nizi". Podatki o natančni lokaciji inštrumenta in točna identifikacija inštrumenta so dodatni podatki, ki se lahko beležijo drugje. 

Agregirane vrednosti se dodajajo nizu z drugo vrednostjo frekvence meritev, vrendosti, ki pripadajo najvišji frekvenci so izmerjene ostale so aggregati iz izmerjenih vrednosti. Določiti je potrebno kodiranje frekvnce ali časovnega koraka. Popravlja se lahko samo izmerjene vrednosti ne direktno izvedenih aggregatov. (Note: Če bi na Zraku želeli popravljati urne vrednsoti se niz meritev in agregatov lahko loči na dva niza ) 

Primarnost  označuje kateri podatek od vseh nizov na lokaciji (isti IDMM) in isti časovni korak (frekvenca meritev) ob določenem času je "glavni", primarnost se določa s proceduro, ki se izvede ob vpisu podatkov in ob izvajanju kontrole, zato se lahko s časom spreminja. Ker je primarnost, določena na nivoju podatkov z istim časovnim korakom, lahko pride do situacije, ko je terminska vrednost za isti termin in različni časovni korak različna, ker dejansko pride iz dveh časovnih nizov.  
Pravtako se lahko na podlagi kontrole spreminja treminska vrednost.

Polje veljavnost podatka bo imelo zakodirano informacijo, ali se ocenjuje ali je podatek veljaven in skozi kakšne kontrole naj bi šel. Način kodiranja še potrebno določit, trenutno se uporablja 15 bitno število. 

Homogenizirani nizi so svoji nizi. Vasako drugo leplenje nizov v nove "sintetične" nize bi se bležilo v drugih tabelah.

**Podatki**

|      |                                    |
| :--- | :--------------------------------- |
| p    | ID_obcine (št_potaje)              |
| p    | IDMM                               |
| p    | par                                |
| p    | zap_st_niza (id_niza)              |
| p    | timestemp                          |
| p    | frekvenca_meritev                  |
|      | term_vrednost                      |
|      | min                                |
|      | t_min                              |
|      | max                                |
|      | t_max                              |
|      | povprecna_vredost                  |
|      | standardni_odklon                  |
|      | {...} ostale_statistike            |
|      | primarnost                         |
|      | natančost                          |
|      | veljavnost podatka (kvaliteta)     |
|      |                                    |
| fk   | st_postaje, IDMM, par, zap_st_niza |

**Nizi**

|      |                       |
| :--- | :-------------------- |
| p    | ID_obcine (st_potaje) |
| p    | IDMM                  |
| p    | par                   |
| p    | zap_st_niza (id_niza) |
|      | tip_postaje           |
|      | zap_st_tipa_postaje   |
|      | cas_zacetek_meritev   |
|      | cas_konca_meritev     |
|      | tip instrumenta       |
|      |                       |
| fk   | st_postaje, IDMM      |

**Merilna mesta**

|      |                              |
| :--- | :--------------------------- |
| p    | ID_obcine (st_potaje)        |
| p    | IDMM                         |
|      | lat                          |
|      | lon                          |
|      | metapodatki o merilnem mestu |  |

TODO:   Določiti način označevanja kvalitete ali natančnosti podatka. Homogenizirani podatki.