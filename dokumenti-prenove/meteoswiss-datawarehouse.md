
- **Q:** How is data transferred from meteorological station to MeteoSwiss? Does stations send files over the network and if which protocol is used. (Our stations sand files over FTP and there is also rsync which is used by department that manages monitoring networks.)  Do you maybe use some other protocol for sending data?   
 **A:** data is get with a call on the station. The collecting central produces then ASCII bulletins which are uploaded to our message handling system.


- **Q:** How is data handled when it gets to your organisation.  In some old (2009) presentation I found that you use ETL system from Informatica (PowerCenter), do you still use this kind of system (commercial ETL, which one) ? Are this initial (real time) hard limit controls implemented in ETL system, and are they run in "transform" phase  before loading into database? Are you limited by what kind of controls you can implement in this stage/system? 
 **A:** We have a convert modul (converting of formats) and a CalcCheck modul (calculating new parameters like QFF, QNH ,… And making hard quality checks (limits,..) which both read and write bulletins. The bulletins are sent afterwards with the message handling system with socket processes in the import directories of the data warehouse system.

- **Q:** In some old (2009) presentation I found that you use ETL system from Informatica (PowerCenter), do you still use this kind of system (commercial ETL, which one) ? Are this initial (real time) hard limit controls implemented in ETL system, and are they run in "transform" phase  before loading into database? Are you limited by what kind of controls you can implement in this stage/system? 
  **A:** We are still using PowerCenter as ETL tool and Oracle as database. Actually we are looking in a lifecyle project what other ETL or database software can be used in future. All our quality tests are not done within the ETL tool. So we are theoretically not limited. After the import of the data, a whole set of quality tests will be done hourly and daily.


- **Q:** For loading 10 min data from your stations network and  initial transform you probably  don't need like real time (Big Data) processing system, but if you would like to load and  process also maybe partner data or crowdsourcing (from personal weather stations)  data, you might need more capacity. How scalable is this part of your system? Have you considered any other system that you later didn't chose to use?
  **A:** We are actually loading data from about 30 partner organisations with more than 3000 stations  (most of them including historical data). The system is so far still good scalable. If we have not enough capacity with an import, we implement an additional import (parallel).  Most of performance problems we can solve with better SQL statements, reorganization of the tabelspaces, or implementing  datamarts and or materialized views.


- **Q:** Data is then loaded into database, are you still using Oracle system? 
  **A:** See above. We are now starting some tests with Postgre.


- **Q:** How is data organised in the database? This is very broad question I know. We have a problem that we have data in too many different tables, because data from new kind of measurements (i.e. automatic stations)  was just added to new table.  Now users who access data directly from the database are complaining that they need to investigate many tables if they are interested in one kind of meteorological variable for example precipitation can be obtained  from automatic station (10 min or old 30 min) or classical main station (1 hour manual measurements) or  precipitation station (24 hour accumulation manual measurement). 
 **A:** We have for all our date 1 table which is partitionated over the time and we only have as Index the primary key (no additional Indexes on the table with the values). All plausibility checks and aggregations are read and write in this table! We sue different parameters for different time granularities. example air temperature: tre200s0 (10’), tre200h0 (hourly mean), tre200hx (hourly maximum), tre200d0 (daily mean), tre200dn (daily minimum), tre200mo (monthly mean), a.s.o.

| key | column                | explanation                                                                                                         |
| --- | --------------------- | ------------------------------------------------------------------------------------------------------------------- |
| p   | REFERENCE_TS          | time of measurement                                                                                                 |
| p   | INSTALLATION_ID       | location of measurement (like a part of a station)                                                                  |
| p   | PARAMETER_ID          | parameter what is measured.                                                                                         |
| p   | MEAS_CAT_NR           | Represents the type/status of a measurement like “official”,                                                        |
|     | LOAD_TS               | time of loading ore updateing of the record                                                                         |
|     | VALUE_NU              | value of measurement: type number                                                                                   |
|     | VALUE_TX              | value of measurement: type string                                                                                   |
|     | UNCERTAINTY_NU        | uncertainty of the measurement                                                                                      |
|     | SHOW_YN               | filter for hiding measurements, e.g. if the sensor has problems)                                                    |
|     | AGG_LOCK_YN           | flag to say if the value can be update with the aggregation or not)                                                 |
|     | AGG_MISSING_VALUES_NU | quality information how many values are missing in the aggregat                                                     |
|     | QC_TREAT_BY_NR        | quality information on what kind of quality tools has checked this value                                            |
|     | MUTATION_INFO_NR      | qulaity information if a value is original or mutated (and how)                                                      |
|     | PLAUSIBILITY_NU       | quality information: Represents the probability of an expert to determine a measurement value as the correct value. |
|     | VALUE_SEQ_ID          | Sequence ID. Is et in the import an when value_nu or value_tx has changed.                                          |

- **Q:** How do you ensure easy access of the data,  do you do  this with interfaces that hide complexity of the data organisation in the database? In your presentation there is also a separation in "data processing and data quality" and "data store and datamarts" . Does the  "data store and datamarts"  have the data that have passed all controls (complex QC test longer interpolation)  and is organised for more easy access? 
 **A:** We have all data in the decscribed table. What we have are defined views for the applications. And a webservice and a R data interface for common requests. For special applications we have datamarts (for example a climat datamart with only daily, monthly and yearly data.


- **Q:** Complex quality test are implemented how? Are this scripts in something like (Perl, python, R...) that read data from database and calculate test?  You run them on the data for past day and then a person/operator does also manual investigation of the suspicious data? (This is also how we do it.)
  **A:** The complex part is written in R. So these special programs can directly read and write data to the database.

- **Q:** Usable Quality information: treatment status bitmap - which kind of quality tool has been executed. ;  If I understand correctly you encode information of the used quality tools (not the individual quality tests) in bitmap. What happens if this tools change, this then changes  the encoding. You don't expect changes, or you change the encoding of all treatment status bitmaps for all data points? 
 **A:** We made categories of quality control tools. So we can change the tool itself. We had to do this because we also have to migrate the old quality informations to the new system.

 | Bit 0                                                                                             | Bit 1                                             | Bit 2                                | Bit 3                                            | Bit 4                                     | Bit 5                                     | Bit 6                                        | Bit 7                                  | Bit 8                                   |
 | ------------------------------------------------------------------------------------------------- | ------------------------------------------------- | ------------------------------------ | ------------------------------------------------ | ----------------------------------------- | ----------------------------------------- | -------------------------------------------- | -------------------------------------- | --------------------------------------- |
 | vertrauenswürdige Quellsysteme (OBS, AWS, NIMDAS, Sondierungen, Phäno, Annalen, ETH, Projekte, …) | autom. Kernprüfung realtime (Calc&Check, CCC, ..) | autom. Kernprüfung stündlich (ACI-H) | autom. Kernprüfung täglich (PuMAB,ACI-D, METEOR) | manuelle Bearbeitung (PuMIB, ICI-Tool, …) | fachliche, komplexe Prüfung Surface Daten | fachliche, komplexe Prüfung Atmosphärendaten | fachliche, komplexe Prüfung Radardaten | fachliche, komplexe Prüfung Modelldaten |
 | BASE_QUALITY_YN                                                                                   | CORE_AUT_T_YN                                     | CORE_AUT_H_YN                        | CORE_AUT_D_YN                                    | MAN_TREAT_YN                              | SURFACE_TEST_YN                           | UPPER_AIR_TEST_YN                            | RADAR_TEST_YN                          | MODEL_TEST_YN                           |

 - **Q:** plausibility is indicated as a value between 0 and 1 ;  If new quality tests are introduced, the calculation of this value will probably change, that means that data from the past will have this value calculated differently?  Are you not concerned with this because this plausibility is kind of a "soft" value it is not meant to have exact meaning for a particular value?. 
  **A:** We store in another table the quality test results. From them we calculate the plausibility. So you are right, this value will change during the time. But we find this correct. Then with the time you improve or add additional quality tests. The idea is that new quality tests can also be used on historical data.
