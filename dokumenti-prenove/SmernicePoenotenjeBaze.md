# Predlog smernic za poenotenje baze meteoroloških podatkov Ameba 

Mnogi zaposleni na ARSO pri svojem delu uporabljamo podatkovno bazo Ameba. V zadnjih letih se je vsebina baze močno razširila – povečalo se je število tabel in podatkov. Pri prehodu s prejšnjega podatkovnega sistema (na strežnikih VAX) so se večinoma prenesle tudi lastnosti tabel, ki so bile takrat določene zlasti z omejitvami pri zapisu podatkov in drugačno filozofijo pri zasnovi baze. Zaradi teh omejitev zapis ni bil poenoten in ta dediščina sedaj predstavlja zmedo pri črpanju podatkov iz Amebe. Ista vrednost neke spremenljivke je lahko zapisana na različne načine, kar otežuje zbiranje in obdelavo podatkov. Oblika tabel, ki se je v nekaterih primerih nespremenjeno prenesla v bazo ameba, velikokrat ne ustreza pravilom relacijskih baz podatkov, zaradi česar je dostop do podatkov in povezovanje tabel oteženo ali v nekaterih primerih nemogoče. Zato predlagamo naslednje smernice za vzpostavitev novih podatkovnih tabel, ki bi sčasoma nadomestile obstoječe:

1.	Vse tabele bi morale biti normalizirane vsaj do stopnje 3.
2.	Poenotenje zapisa datuma in ure v obliko datuma z uro (timestamp; recimo 2005-05-17 12:30:00), pri čemer naj bo časovni pas za vse tabele enak (srednjeevropski čas). Ločitev datuma od ure je zelo nepraktična.
3.	Enake enote za isto spremenljivko (°C za temperaturo, mm za višino padavin, W/m2 za gostoto sevalnega toka)  
4.	Onemogočen vnos vrednosti daleč izven fizikalno mogočih mej (na primer temperature zraka 2 metra nad tlemi pod –60 °C ali nad +50 °C). Najbolj praktično bi bilo meje dodati že v same definicije tabel.
5.	En stolpec za eno spremenljivko (recimo urno višino padavin ali urno vrednost temperature). To je povezano z normalizacijo tabel, eno od lastnosti tabel v pravilno zasnovani relacijski bazi (točka 1).
6.	Enoten označevalec postaje pri podatkih iz različnih virov (recimo id ali številka postaje). ID v povezavi s številko parametra meritve to meritev popolnoma definira. 
7.	V vseh tabelah enak znak za manjkajoč podatek – NULL.
8.	Vnos vrednosti za celotno obdobje delovanja postaje po metapodatkih, od začetka do zadnjega podatka. Termini brez meritev bi morali biti vneseni, vrednosti meritev pa označene z NULL.
9.	Če je mogoče, naj bodo združeni podatki iste vrste v eno samo tabelo (recimo urne vrednosti temperature iz AMP postaj, elektronskih registratorjev itn.)
10.	Izdelati smernice in pravila za izdelavo tabel, ki bi se jih moral držati vsak, ki bi kreiral tabele v bazi.
11.	Dokumentirati bazo.
12.	Omejiti število tabel v bazi. Za testne stvari bi lahko izdelali novo bazo. Število tabel v zdajšnji bazi je zelo nepregledno.
13.	Dodati vezi oz. meje v tabele, ki bi onemogočile vstavljanje nesmiselnih podatkov.
14.	Skrbno izbrati podatkovne tipe spremenljivk (celoštevilčnim spremenljivkam nima smisla dodeliti tipa s premikajočo vejico).
15.	V bazo dodati funkcije, ki bi olajšale delo s tabelami.


*Pripravila: Gregor Vertačnik in Renato Bertalanič, april 2015*
