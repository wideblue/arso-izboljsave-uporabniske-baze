# Prenova spletne aplikacije za dostop do arhivskih podatkov

Aplikacija [arhiv meritev], ki je namenjena dostopanju do arhivkih meteoroloških podatkov preko spleta, je stara in zato problematična iz vidika vdzrževanja. Na podlagi pogovorov z uporabniki aplikacije ocenjujemo, da uporabniška izkušnja z aplikacijo ni povsem zadovoljiva. Pripravili smo specifikacijo za izboljšano uporabniško izkušnjo. 

## Specifikacije uporabniške izkušnje

Uporabnik, ki želi priti do določenih meteoroloških podatkov, mora skozi interakcijo z aplikacijo določiti sledeče parametre izbora 

* meteorološke spremeljivke (npr. temperatura, vlaga, višina padavin ....)
* lokacijo (za katero želimo podatke)
* časovno obdobje 
* časovni interval (resolucija) podatkov (npr. 10 min, dnevne, mesečne ...)

Parametri iskanja so medsebojno odvisni, ker na vseh lokacijah nimamo meritev vseh spremenljivk  v vseh obdobjih in v vseh časovnih intervalih. Uporabniku želimo omogočit, da na intuitiven način pride do podakov, ki so hkrati na razpolago in kar najbolj ustrezajo njegovim potrebam.

Kot primer dobre uporabniške izkušnje smo identificirali [Kanadske strani](http://climate.weather.gc.ca/historical_data/search_historic_data_e.html) z zgodovinskimi meteorološkimi podatki, zato se ta specifikacija uporabiške izkušnje zelo zgleduje po izkušnji, ki jo ponujajo omenjene strani. 

### Vstopni pogled/stran

Na prvem pogledu uporabnik kot parametre iskanja vnese lokacijo in časovno obdobje ter opcijsko meteorološke spremenljivke, ki ga zanimajo. Če uporabnik sam ne izbere meteoroloških spremenljivk, se privzame standardni nabor spremenljik. *TODO: Katere spremenljivke predstavljajo standardni nabor, bo še določeno.*    
 
**Izbor lokacije:**
 Uporabnik lahko izbira lokacijo na dva izključujoča načina 

- [x] Izbor po imenu kraja (predefiniran izbor krajev)
- [ ] Vnos koordinat (vnos vrednosti v spletno formo ali morda izbor na karti) 

Ker podatkov za določen kraj morda ni na razpolago uporabnik določi tudi radij oddaljenosti od izbrane lokacije, ki določi območje znotraj katerega lahko iščemo lokacije za katere so podatki na voljo.

**Izbor časovnega obdobja:**
Uporabnik lahko izbira časovno obdobje na dva izključujoča načina:

- [ ] Začetno in končno leto 
- [x] Izbor določenega datuma

**Izbor meteoroloških spremenljivk:**
Uporabnik lahko opcijsko označi spremenljivke, ki ga zanimajo ali odoznači privzet nabor spremenljik ali statistik. V obstoječi aplikaciji [arhiv meritev] imamo na voljo kar zajeten [nabor spremenljivk in statistik](SeznamMeteoroloskihSpremenljivk.md)   

*TODO: Ker je nabor meteoroloških spremenljik odvisen od časovnega obdobja je potrebno razmislit  ali se uporabniku omogoči poljuben ali se izbor kako omeji*


### Seznam postaj (drugi pogled/stran) 
Po izboru lokacije, časovnega obdobja in morda meteoroloških spremeljivk se uporabniku prikaže seznam postaj, ki ustrezajo začetni izbiri, oziroma so dovolj blizu izbrane lokacije in imajo znotraj izbranega obdobja na voljo podatke za izbrane spremenljivke. 

Pogoji razvrstitev rezultatov (*še za razmislit*):

1. začne z najbljižjo postajo 
2. najdaljšim nizi z "iste" lokacije zgoraj

Postaje so prikazane v tabeli s sledečimi stolpci

1. Ime postaje.
2. Izbirnik s časovnimi intervali, ki so na voljo (standardna izbira: najnižji interval).
3. Izbirnik z leti, ki so na voljo (standardna izbira: prvo leto podatkov?).
4. Izbirnik z meseci ki so na voljo (standardna izbira: prvi mesec v izbranem letu s podatkov?).
5. Izbirnik z dnevi ki so na voljo (standardna izbira: prvi dan v izbranem mesecu s podatkov?).
6. Gumb "Izberi".

**Izbor časovnega intervala/resolucije:**
Naši podatki so lahko s sledečimi časovnimi intervali:

* 10 min
* 30 min
* *1 ura - nimamo, ali bi morda imeli?*
* terminske - *za razmislit ali imamo ta intervala ali vrednosti ob 7, 14 in 21ih damo kot možne spremenljivke pri dnevnem intervalu* 
* dnevne
* mesečne
* letne

Kanadske strani imajo samo urne, dnevne in mesečne.  Če imamo podatke v višji časovni resoluciji se lahko naračuna podetke za vse nižje resolucije (*ali se izračunajo sproti ali so naračunani v naprej je še odprto*). Izbirnik s časovnimi intervali ima za določeno postajo vse intervale od najmanjšega, ki je na voljo za to postajo do največjega (letne). To tudi pomeni, da se samodejna in klasična postaja obravnavata ločeno (vsaka v svoji vrstici) in da lahko uporabnik pride do dveh različnih vrednosti za isto spremenljivko (dnevno, mesečno, letno) na "isti" lokaciji. To je sicer v nasprotju s tem, da bi uporabniku ponudili samo eno vrednost na eni lokaciji, je pa skladno z realnostjo, ker gre v ozadju dejasko za več neodvisnih meritev. #3    

Glede na izbor časovnega intervala lahko postanejo izbirniki za mesec in dan neaktivni. Če je časovni interval leto sta neaktivna oba izbirnika, če je interval mesec potem je neaktiven izbirnik za dan. Izbira časovnega intervala tako vpliva na dolžino časovnega obdobje podatkov, ki bodo prikazani hkrati po pritisku gumba "Izberi" . Če bo izbran letni interval, bodo prikazani podatki za vsa leta, ki so na voljo, če bo mesečni interval, bo prikazano leto podatkov, če bo dnevni interval, bo prikazan mesec podatkov, če bo urni intrval ali manj, bo prikazan dan podatkov.   

### Prikaz podatkov (tretja stran/pogled)

Po pritisku na gumb "Izberi" so izbrani vsi parametri, ki so potrebni za pridobitev in prikaz podatkov. Podatki so prikazani v tabeli, kjer je v prvem stolpecu čas v ostalih pa izbrane meteorološke spremenljivke, v vrsticah pa si z izbranim časovnim intervalom sledijo podatki. 

Nad tabelo so sledeče interaktivne komponente uporabniškega vmesnika:

1. Gumb za premik za eno časovno obdobje nazaj, če je na voljo (en dan, ali en mesec ali leto odvisno od izbane časovne resolucije podatkov ).
2. Gumb za izris grafa s podatki.
3. Izbirnik za časovno obdobje (ali se izbira poleg leta tudi mesec in dan je odvisno od izbane časovne resolucije podatkov ).
4. Izbirnik za prikazane meteorološke spremenljivke, ki so na voljo v tej časovni resoluciji (moznih več izbir).
5. Gumb za prenos podatkov v tekstovni datoteki (csv, tsv) na računalnik. V datoteki so lahko podatki za eno stopnjo višje obdobje kot so prikazani v tabeli  (resolucija urna ali manj -> obdobje 1 mesec, resolucija dan -> obdobje 1 leto).
6. Gumb za premik za eno časovno obdobje naprej, če je na voljo (en dan, ali en mesec ali leto odvisno od izbane časovne resolucije podatkov).

**Spreminjanje časovnega intervala/resolucije podatkov:**
Kadar so na voljo podatki tudi v višji resoluciji, lahko s klikom na čas v prvem stolpcu tabele preidemo na prikaz podatkov v višji resoluciji (npr. gledamo dnevne vrednosti in s klikom na določen dan pridemo do podatkov na ta dan v urni resoluciji - *kaj narediti s pod urno resolucijo?*)
S spremembo resolucije se spremeni tudi izbor meteoroloških spremenljivk, ki so prikazane. - *TODO: še pregledat in razmislit*

[arhiv meritev]: http://meteo.arso.gov.si/met/sl/archive/