## Samodejne postaje meteorološke spremenljivke - polurni podatki / 10 min
 *  povprečen zračni tlak (hPa) 
 *  minimalna relativna vlaga (%) 
 *  minimalen zračni tlak (hPa) 
 *  maksimalna relativna vlaga (%) 
 *  maksimalen zračni tlak (hPa) 
 *  količina padavin (mm) 
 *  terminska temperatura zraka na 2m (°C) 
 *  povprečna hitrost vetra (m/s) 
 *  povprečna temperatura zraka na 2m (°C) 
 *  povprečna smer vetra (°) 
 *  minimalna temperatura zraka na 2m (°C) 
 *  maksimalna hitrost vetra (m/s) 
 *  maksimalna temperatura zraka na 2m (°C) 
 *  povprečen energijski tok globalnega sevanja (W/m2) 
 *  terminska relativna vlaga (%) 
 *  povprečen energijski tok difuznega sevanja (W/m2) 
 *  povprečna relativna vlaga (%)

## Termiske prikaz vremena   7ih, 14ih, 21ih    

* Oblačnost
* Temperatura
* Vlaga
* Tlak
* Hitrost vetra
* Smer vetra
* Stanje tal

#### Terminske prikaz vremena dan 

* Minimalna temperatura
* Maksimalna temperatura
* Povprečna temperatura (klimatološko povprečje)
* Minimalna temperatura na 5 cm
* Trajanje sončnega obsevanja
* Padavine ob 7. uri za zadnjih 24 ur
* Višina novega snega ob 7. uri za zadnjih 24 ur
* Višina snežne odeje izmerjena ob 7. uri
* Pojavi

## Dnevni podatki 

#### pogosto uporabljene meteorološke spremenljivke

 * povprečna temperatura zraka na 2 m (°C) 
 * povprečna oblačnost (pokritost neba v %) 
 * maksimalna temperatura zraka na 2 m (°C) 
 * nevihta 
 * minimalna temperatura zraka na 2 m (°C) 
 * grmenje 
 * 24-urna količina padavin ob 7 h (mm) 
 * bliskanje 
 * skupna višina snežne odeje (cm) 
 * toča 
 * višina novozapadlega snega (cm) 
 * viharni veter (> 8bf) 
 * trajanje sončnega obsevanja (h) 

#### terminske meritve ob 7h, 14h in 21h


 * temperatura zraka na višini 2 m (°C)
 * smer vetra
 * relativna vlaga (%)
 * hitrost vetra (m/s)

#### vse dnevne meteorološke spremenljivke
 * povprečna temperatura zraka na 2 m (°C)
 * sneg
 * maksimalna temperatura zraka na 2 m (°C)
 * zrnat sneg
 * minimalna temperatura zraka na 2 m (°C)
 * ploha snega
 * minimalna temperatura zraka na 5cm (°C)
 * dež s snegom
 * povprečna hitrost vetra (m/s)
 * babje pšeno
 * povprečna oblačnost (pokritost neba v %)
 * ploha dežja s snegom
 * povprečna relativna vlaga (%)
 * toča
 * povprečen zračni tlak (hPa)
 * sodra
 * 24-urna količina padavin ob 7 h (mm)
 * megla
 * skupna višina snežne odeje (cm)
 * megla z vidnim nebom
 * višina novozapadlega snega (cm)
 * ledena megla
 * trajanje sončnega obsevanja (h)
 * meglica
 * močan veter (> 6bf)
 * suha motnost
 * viharni veter (> 8bf)
 * nizka (talna) megla
 * dež
 * rosa
 * rosenje
 * slana
 * ploha dežja
 * poledica
 * nevihta
 * poledica na tleh
 * grmenje
 * ivje
 * bliskanje
 * trdo ivje
 * dež, ki zmrzuje
 * padavine
 * rosenje, ki zmrzuje
 * snežna odeja
 * ledene iglice

## Mesečni ali letni podatki
### Pogosto uporabljene meteorološke statistike
 * povprečna temperatura zraka na 2 m (°C)
 * povprečna oblačnost
 * povprečna maksimalna temperatura na 2 m (°C)
 * število dni z nevihto
 * povprečna minimalna temperatura na 2 m (°C)
 * število dni s padavinami nad 0.1 mm
 * količina padavin (mm)
 * število dni s snežno odejo
 * trajanje sončnega obsevanja (h)

### Vse meteorološke statistike
 * povprečna temperatura zraka na 2 m ob 7 h (°C)
 * količina padavin (mm)
 * povprečna temperatura zraka na 2 m ob 14 h (°C)
 * maksimalna dnevna količina padavin (mm)
 * povprečna temperatura zraka na 2 m ob 21 h (°C)
 * datum z maksimalno dnevno količino padavin
 * povprečna temperatura zraka na 2 m (°C)
 * maksimalna višina snežne odeje (cm)
 * povprečna maksimalna temperatura na 2 m (°C)
 * datum z maksimalno višino snežne odeje
 * absolutna maksimalna temperature na 2 m (°C)
 * število mrzlih dni (minimalna temperatura ≤ -10 °C)
 * datum z absolutno maksimalno temperaturo na 2 m
 * število ledenih dni (maksimalna temperatura < 0 °C)
 * povprečna minimalna temperatura na 2 m (°C)
 * število hladnih dni (minimalna temperatura < 0 °C)
 * absolutna minimalna temperature na 2 m (°C)
 * število toplih dni (maksimalna temperatura ≥ 25 °C)
 * datum z absolutno minimalno temperaturo na 2 m
 * število vročih dni (maksimalna temperatura ≥ 30 °C)
 * povprečna minimalna temperatura na 5cm (°C)
 * število toplih noči (minimalna temperatura ≥ 20 °C)
 * absolutna minimalna temperature na 5cm (°C)
 * število dni z močnim vetrom (> 6 bf)
 * datum z absolutno minimalno temperaturo na 5cm
 * število dni z viharnim vetrom (> 8 bf)
 * povprečna relativna vlaga ob 7 h (%)
 * število dni z nevihto
 * povprečna relativna vlaga ob 14 h (%)
 * število dni s padavinami nad 0.1 mm
 * povprečna relativna vlaga ob 21 h (%)
 * število dni s padavinami nad 1 mm
 * povprečna relativna vlaga (%)
 * število dni s padavinami nad 10 mm
 * absolutna minimalna relativne vlage (%)
 * število dni s padavinami nad 20 mm
 * datum z absolutno minimalno relativno vlago
 * število dni z dežjem nad 0.1 mm
 * povprečna oblačnost ob 7 h (pokritost neba v %)
 * število dni z dežjem in snegom nad 0.1 mm
 * povprečna oblačnost ob 14 h (pokritost neba v %)
 * število dni s snegom nad 0.1 mm
 * povprečna oblačnost ob 21 h (pokritost neba v %)
 * število dni s snežno odejo
 * povprečna oblačnost
 * število jasnih dni (oblačnost < 20%)
 * povprečen zračni tlak ob 7 h (hPa)
 * število oblačnih dni (oblačnost > 80%)
 * povprečen zračni tlak ob 14 h (hPa)
 * število dni z meglo
 * povprečen zračni tlak ob 21 h (hPa)
 * število dni s točo
 * povprečen zračni tlak (hPa)
 * število dni z relativno vlago pod 30%
 * povprečna hitrost vetra (m/s)
 * število dni z relativno vlago pod 50%
 * trajanje sončnega obsevanja (h)
 * število dni z relativno vlago nad 80%


